# Report

## A. Overall Process

### Timeline
- 18th December 2018 (From 2pm to 7pm - Hanoi time):
  - Researched (and tried) free NodeJS libraries to get a list of all stock symbols and stock price.
  - Configured dependencies and set up working environment based on the stack that I have been familiar with (NodeJS, AngularJS, Redis).
  - Created basic front-end page layout.
  - Added feature showing a list of all stock symbols and showing current price of the selected symbol.
  
- 19th/20th December 2018 (From 11pm to 5am - Hanoi time):
  - Re-created the feature listing all symbols as the list is too large that led to very slow responses. They should be classified by Initial.
  - Added feature showing current user's balance
  - Added feature to deposit/withdraw USD to/from user account
  - Added feature showing all deposit/withdraw history (up to 100 entries)
  - Added feature to buy/sell stock (Notify when balance is not enough)
  - Added feature showing all buy/sell activity history (up to 100 entries)
  - Added news navigation bar to have more interesting page
  
- 21th December 2018 (From 1am to 3am - Hanoi time);
  - Wrote README.md and REPORT.md 

Total development time: approximately 11 hours
       
### Key decisions
- Used stack combination of NodeJS for back-end, Redis for quick-implement database, AngularJS for front-end
- Used "stock-symbol-lookup" library to get list of stock symbols and "yahoo-stock-prices" library to get stock price
- Used Redis to store user data so that the data remain available (not reset) even if the back-end is restarted.
- Broke the list of all symbols into smaller lists (based on different initials) for faster responses.

## B.Shortcuts, assumptions, and known bugs
- There is only one user, thus I did not implement login function. However, a pin/passcode could be implemented in the future to make sure only the owner can access the page.
- Since I used the free API library to get stock price, the response is quite slow. We could improve it by purchasing better API subscription.
- In front-end, the panels (balance, history, ...) can be dragged/rearranged to new positions according to user need.
- The GUI only shown well in Google Chrome (has not tried with other browsers). 
- Known bugs: If the user buys then sells the same amount of a stock. Its zero balance is still shown in the user balance table which should be removed. This can be solved by creating a "Hide small" button.

## C.What slowed me down, what was unexpected
- Finding the right API for getting stock data is quite time-consuming, especially under a short time constraint.
- The list of all stocks is much larger than I thought.
- I sometimes took too much time on the front-end trying to make it look as best as possible. However, it is not the main purpose of this task 

## D.Problems I ran into, and how I solved them
- Some API for getting stock data did not work as documented (probably they were developed long time ago and has not been maintained).
I had to keep trying and testing the better one.
- Again, the list of all stocks is too large. I had to split them into different groups based on initials.

