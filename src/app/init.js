/**
 * Created by trung.than on 2018/12/18.
 */

const log4js = require('log4js');
log4js.configure({
    "appenders": [
        {"type": "console"},
        {"type": "dateFile",
            "filename": "logs/app.log",
            "pattern": "-yyyy-MM-dd",
            "alwaysIncludePattern": true}
    ]
});
const log = log4js.getLogger('app');

log.info(" ======================== Application init ==========================================");

require('./../lib/prototype.custom');
const path = require("path");
const config = require('config');
const redis_promise_controller = require('../controllers/redis_promise_controller');

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
// require('events').EventEmitter.prototype._maxListeners = 100000;

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
//CORS middleware
const allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
};

app.use(allowCrossDomain);

//socket.io
const http = require('http').Server(app);
const io = require('socket.io')(http);
const redisAdapter = require('socket.io-redis');
io.adapter(redisAdapter(config.get('redis')));

io.of('/').adapter.on('error', function(err){
    log.error('Redis Adapter: ', err)
});

// // implelemt websocket server
// const WebSocketServer = require('ws').Server,
//     wss = new WebSocketServer({port: 4010});

http.listen(config.get('port.app'), function () {
    log.info('listening on *:', config.get('port.app'));
});

// socket.io message control
require('./routes/socket-routes')(io);
// require('./routes/routes')(app);
// require('./routes/ws-routes')(wss);

app.use('/static', express.static(__dirname + '/../client/static'));
app.use('/trade', express.static(__dirname + '/../client/trade'));

app.get('/not_found', function (req, res) {
    res.sendFile(path.join(__dirname + '/../client/not_found.html'));
});
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/../client/trade.html'));
});
app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname + '/../client/not_found.html'));
});


/* ======================= Get stock data ================================*/
const StockSymbolLookup = require('stock-symbol-lookup');
log.info('Start getting stock data');

const symbol_list_key = config.get('redis_chanel.symbol_list');

const getSymbolList = function() {
    StockSymbolLookup.loadData()
        .then(async (data) => {
            //return if data is not correct
            if (!data || !data.securities || data.securities.length < 1)
                return;

            let promises = [];
            for (let groups of data.symbols ) {
                if (groups.length < 1)
                    continue;

                // Create Redis key based on initial of each group
                let key = symbol_list_key + '_' + groups[0].symbol.charAt(0);

                //Store all symbol to Redis
                let symbol_obs = [];
                for (let symb of groups ) {
                    //log.info('---', symb);
                    symbol_obs.push({
                        symbol: symb.symbol,
                        company: (symb.securityName) ? symb.securityName.split('-')[0] : ''
                    });
                }

                promises.push(redis_promise_controller.set(key, JSON.stringify(symbol_obs)));
            }

            await Promise.all(promises);
            log.info('Setting symbol list to Redis DONE');
        })
        .catch((error) => {
            log.error("Init - Getting symbol list error: ", error);
        });
};

setInterval(getSymbolList, 60 * 60 * 1000); //60 minutes
getSymbolList();
