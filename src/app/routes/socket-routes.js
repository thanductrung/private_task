'use strict';

const yahooStockPrices = require('../../yahoo-stock-prices/yahoo-stock-prices');
const redis_promise_controller = require('../../controllers/redis_promise_controller');
const symbol_controller = require('../../controllers/symbol_controller');
const utils = require('../../lib/utils');
const config = require('config');

const log4js = require('log4js');
log4js.configure({
    "appenders": [
        {"type": "console"},
        {"type": "dateFile",
            "filename": "logs/socket-router.log",
            "pattern": "-yyyy-MM-dd",
            "alwaysIncludePattern": true}
    ]
});
const log = log4js.getLogger('socket-router');

module.exports = async function(io) {
    io
		.on('connect', function (socket) {
			log.info(socket.id, 'connected');

            //==== symbol_list request
            socket.on('symbol_list', async (msg, fn) => {
                log.info("received symbol_list request");
                let symbol_list = [];

                try {
                    let symbol_list_key = config.get('redis_chanel.symbol_list') + "_" + msg["initial"];
                    let symbol_list_value = await redis_promise_controller.get(symbol_list_key);

                    if (symbol_list_value)
                        symbol_list = JSON.parse(symbol_list_value);

                    //log.info('symbol_list', symbol_list);

                    //Return data to client
                    fn({result_code: 0, data: symbol_list});
                }
                catch (err) {
                    log.error('symbol_list', err);

                    if(!err.value)
                        err = utils.error.UNKNOWN_ERR;

                    fn({result_code: err.value, error_message: err.key});
                }
            });

            //==== fund_history request
            socket.on('fund_history', async (msg, fn) => {
                log.info("received fund_history request");
                let fund_history = [];

                try {
                    const dep_wdr_history = config.get('redis_chanel.dep_wdr_history');
                    let redis_value = await redis_promise_controller.list(dep_wdr_history, {from: 0, to: -1});

                    if (redis_value && redis_value.length > 0) {
                        for (let item of redis_value) {
                            fund_history.push(JSON.parse(item));
                        }
                    }

                    //Return data to client
                    fn({result_code: 0, data: fund_history});
                }
                catch (err) {
                    log.error('fund_history', err);

                    if(!err.value)
                        err = utils.error.UNKNOWN_ERR;

                    fn({result_code: err.value, error_message: err.key});
                }
            });

            //==== purchase_history request
            socket.on('purchase_history', async (msg, fn) => {
                log.info("received purchase_history request");
                let purchase_history = [];

                try {
                    const purchase_history_key = config.get('redis_chanel.purchase_history');
                    let redis_value = await redis_promise_controller.list(purchase_history_key, {from: 0, to: -1});

                    if (redis_value && redis_value.length > 0) {
                        for (let item of redis_value) {
                            purchase_history.push(JSON.parse(item));
                        }
                    }

                    //Return data to client
                    fn({result_code: 0, data: purchase_history});
                }
                catch (err) {
                    log.error('purchase_history', err);

                    if(!err.value)
                        err = utils.error.UNKNOWN_ERR;

                    fn({result_code: err.value, error_message: err.key});
                }
            });

            //==== get_currency_symbol_price request
            socket.on('get_currency_symbol_price', async (msg, fn) => {
                log.info("received get_currency_symbol_price request");
                const current_symbol_key = config.get('redis_chanel.current_symbol');
                const defaut_symbol = config.get('default_symbol');

                try {
                    let current_symbol = await redis_promise_controller.get(current_symbol_key);
                    if (!current_symbol)
                        current_symbol = defaut_symbol;
                    else
                        current_symbol = JSON.parse(current_symbol);

                    // Get current price from Yahoo Finance
                    yahooStockPrices.getCurrentPrice(current_symbol.symbol, function (err, price) {
                        if (err) {
                            log.error('Getting price for', current_symbol.symbol, 'ERROR', err);
                            fn({result_code: 1,
                                error_message: "Price not available for this symbol",
                                data: {
                                    symbol: current_symbol.symbol,
                                    company: current_symbol.company,
                                    price: null
                                }
                            });
                            return;
                        }

                        //Return data to client
                        fn({result_code: 0, data: {
                                symbol: current_symbol.symbol,
                                company: current_symbol.company,
                                price: price
                            }
                        });
                    });
                }
                catch (err) {
                    log.error('get_currency_symbol_price', err);

                    if(!err.value)
                        err = utils.error.UNKNOWN_ERR;

                    fn({result_code: err.value, error_message: err.key});
                }
            });

            //==== set_currency_symbol request
            socket.on('set_currency_symbol', async (msg, fn) => {
                log.info("received set_currency_symbol request");
                const current_symbol_key = config.get('redis_chanel.current_symbol');

                try {
                    await redis_promise_controller.set(current_symbol_key, JSON.stringify(msg));

                    // Get current price from Yahoo Finance
                    yahooStockPrices.getCurrentPrice(msg.symbol, function (err, price) {
                        if (err) {
                            log.error('Getting price for', msg.symbol, 'ERROR', err);
                            fn({result_code: 1,
                                error_message: "Price not available for this symbol",
                                data: {
                                    symbol: msg.symbol,
                                    company: msg.company,
                                    price: null
                                }
                            });
                            return;
                        }

                        //Return data to client
                        fn({result_code: 0, data: {
                                symbol: msg.symbol,
                                company: msg.company,
                                price: price
                            }
                        });
                    });
                }
                catch (err) {
                    log.error('set_currency_symbol', err);

                    if(!err.value)
                        err = utils.error.UNKNOWN_ERR;

                    fn({result_code: err.value, error_message: err.key});
                }
            });

            let calculate_usd_balance = async function(balance) {
                let promises = [];
                for (let symb of balance) {
                    if (symb.symbol !== "USD") {
                        promises.push(symbol_controller.get_symbol_price({symbol:symb.symbol})
                            .then(function(price) {
                                symb.usd_balance = symb.balance * price;
                            })
                            .catch(function(err){
                                log.error("Get Yahoo price for", symb.symbol, "ERROR: ", err);
                                symb.usd_balance = 0;
                            })
                        );
                    }
                }

                await Promise.all(promises);
            };

            //==== get_balance_list request
            socket.on('get_balance_list', async (msg, fn) => {
                log.info("received get_balance_list request");
                const balance_key = config.get('redis_chanel.balance');

                try {
                    let balance = await redis_promise_controller.get(balance_key);
                    if (!balance)
                        balance = [{symbol: "USD", balance: 0, usd_balance: 0}];
                    else
                        balance = JSON.parse(balance);

                    /* Convert balance to USD value */
                    await calculate_usd_balance(balance);

                    //Return data to client
                    fn({result_code: 0, data: balance});
                }
                catch (err) {
                    log.error('get_balance_list', err);

                    if(!err.value)
                        err = utils.error.UNKNOWN_ERR;

                    fn({result_code: err.value, error_message: err.key});
                }
            });

            //==== update_usd_balance request
            socket.on('update_usd_balance', async (msg, fn) => {
                log.info("received update_usd_balance request");
                const balance_key = config.get('redis_chanel.balance');
                const history_length = config.get("history_length");
                const dep_wdr_history = config.get('redis_chanel.dep_wdr_history');

                try {
                    let balance = await redis_promise_controller.get(balance_key);
                    if (!balance) {
                        if (msg.amount < 0)
                            throw utils.error.NOT_ENOUGH_BALANCE;

                        balance = [{symbol: "USD", balance: msg.amount, usd_balance: msg.amount}];
                    }
                    else {
                        balance = JSON.parse(balance);

                        //Check if withdraw too much
                        let usd_obj = balance.getItemBy("symbol", "USD");
                        if (usd_obj.balance + msg.amount < 0)
                            throw utils.error.NOT_ENOUGH_BALANCE;

                        //Update new USD balance
                        usd_obj.balance += msg.amount;
                        usd_obj.usd_balance += msg.amount;
                    }

                    //Store history to Redis
                    redis_promise_controller.lpush(dep_wdr_history, JSON.stringify({
                        type: (msg.amount> 0) ? "Deposit" : "Withdraw",
                        amount: Math.abs(msg.amount),
                        time: Date.now()
                    }));
                    redis_promise_controller.ltrim(dep_wdr_history, {from: 0, to: history_length - 1});

                    // /* Convert other symbol balance to USD value */
                    // await calculate_usd_balance(balance);

                    //Store new balance to Redis
                    await redis_promise_controller.set(balance_key, JSON.stringify(balance));

                    //Return data to client
                    fn({result_code: 0, data: balance});
                }
                catch (err) {
                    log.error('get_balance_list', err);

                    if(!err.value)
                        err = utils.error.UNKNOWN_ERR;

                    fn({result_code: err.value, error_message: err.key});
                }
            });

            //==== purchase request
            socket.on('purchase', async (msg, fn) => {
                log.info("received purchase request");
                const balance_key = config.get('redis_chanel.balance');
                const purchase_history_key = config.get('redis_chanel.purchase_history');
                const history_length = config.get("history_length");

                try {
                    let balance = await redis_promise_controller.get(balance_key);
                    if (!balance)
                        throw utils.error.NOT_ENOUGH_BALANCE;

                    balance = JSON.parse(balance);

                    if (msg.type==="buy") {
                        let usd_obj = balance.getItemBy("symbol", "USD");
                        if (!usd_obj || (usd_obj.balance < msg.amount * msg.price))
                            throw utils.error.NOT_ENOUGH_BALANCE;

                        usd_obj.balance -= msg.amount * msg.price;
                        usd_obj.usd_balance -= msg.amount * msg.price;

                        let stock_obj = balance.getItemBy("symbol", msg.symbol);
                        if (!stock_obj) {
                            balance.push({
                                symbol: msg.symbol,
                                balance: msg.amount,
                                usd_balance: msg.amount * msg.price
                            });
                        }
                        else {
                            stock_obj.balance += msg.amount;
                            stock_obj.usd_balance += msg.amount * msg.price;
                        }
                    }
                    else if (msg.type==="sell") {
                        let stock_obj = balance.getItemBy("symbol", msg.symbol);
                        if (!stock_obj || (stock_obj.balance < msg.amount))
                            throw utils.error.NOT_ENOUGH_BALANCE;

                        stock_obj.balance -= msg.amount;
                        stock_obj.usd_balance -= msg.amount * msg.price;

                        let usd_obj = balance.getItemBy("symbol", "USD");
                        if (!usd_obj) {
                            balance.push({
                                symbol: "USD",
                                balance: msg.amount * msg.price,
                                usd_balance: msg.amount * msg.price
                            });
                        }
                        else {
                            usd_obj.balance += msg.amount * msg.price;
                            usd_obj.usd_balance += msg.amount * msg.price;
                        }
                    }
                    else
                        throw utils.error.WRONG_PURCHASE_TYPE;


                    //Store history to Redis
                    redis_promise_controller.lpush(purchase_history_key, JSON.stringify({
                        type: (msg.type==="buy") ? "Buy" : "Sell",
                        amount: msg.amount,
                        symbol: msg.symbol,
                        price: msg.price,
                        time: Date.now()
                    }));
                    redis_promise_controller.ltrim(purchase_history_key, {from: 0, to: history_length - 1});

                    // /* Convert other symbol balance to USD value */
                    // await calculate_usd_balance(balance);

                    //Store new balance to Redis
                    await redis_promise_controller.set(balance_key, JSON.stringify(balance));

                    //Return data to client
                    fn({result_code: 0, data: balance});
                }
                catch (err) {
                    log.error('get_balance_list', err);

                    if(!err.value)
                        err = utils.error.UNKNOWN_ERR;

                    fn({result_code: err.value, error_message: err.key});
                }
            });

            //==== get news request
            socket.on('news', async (msg, fn) => {
                try {
                    log.info("news", JSON.stringify(msg, null, "\t"));
                    let news = await redis_promise_controller.get(config.get('redis_chanel.news_feed'));
                    msg.result_code = 0;
                    msg.error_message= '';
                    msg.data = JSON.parse(news);

                    fn(msg);
                }
                catch (err) {
                    log.error('news', err);
                    fn({result_code: err.value, error_message: err.key});
                }
            });
        })
};