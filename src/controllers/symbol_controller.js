'use strict';
const Promise = require("bluebird");
const yahooStockPrices = require('../yahoo-stock-prices/yahoo-stock-prices');

exports.get_symbol_price =function(req) {
    /*
        symbol  String
     */
    return new Promise(async (resolve, reject) => {
        // Get current price from Yahoo Finance
        yahooStockPrices.getCurrentPrice(req.symbol, function (err, price) {
            if (err) {
                reject(err);
                return;
            }

            resolve(price);
        });
    });
};