/**
 * Created by trung.than on 2018/12/18.
 */

"use strict";

// init config
const config = require('config');
const log = require('log4js').getLogger("redis_promise_controller");
const bluebird = require('bluebird');
// init redis
const redis = require("redis");
const redis_cli = redis.createClient(config.get('redis.port'), config.get('redis.host'), {password: config.get('redis.password')});
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

redis_cli.on("error", function (err) {
    log.error("Error " + err);
});

exports.list = function (key, options) {
    return redis_cli.lrangeAsync(key, options.from, options.to);
};

exports.ltrim = function (key, options) {
    return redis_cli.ltrimAsync(key, options.from, options.to);
};

exports.get = function (key) {
    return redis_cli.getAsync(key);
};

exports.set = function (key, data) {
    return redis_cli.setAsync(key, data);
};

exports.hgetall = function (key) {
    return redis_cli.hgetallAsync(key);
};

exports.hget = (key, subkey) => {
    return redis_cli.hgetAsync(key, subkey);
};

exports.hmset = function (key, data) {
    return redis_cli.hmsetAsync(key, data);
};

exports.expire = function (key, timeInSeconds) {
    return redis_cli.expireAsync(key, timeInSeconds);
};

exports.publish = function (key, data) {
    return redis_cli.publishAsync(key, data).then(function(res) {
        return res;
    });
};

exports.del = function (key) {
    redis_cli.delAsync(key).then(function(res) {
        return res;
    });
};

exports.rpush = function (key, data) {
    redis_cli.rpushAsync(key, data);
};

exports.lpush = function (key, data) {
    redis_cli.lpushAsync(key, data);
};

exports.create_client = function(){
    let client = redis.createClient(config.get('redis.port'), config.get('redis.host'), {password: config.get('redis.password')});
    client.on("error", function (err) {
		log.error("Error " + err);
	});

	return client;
};

exports.get_client = () => {
    return redis_cli;
};