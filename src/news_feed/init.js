
const config = require('config');
const log = require('log4js').getLogger("socket-routes");
const redis_promise_controller = require('../controllers/redis_promise_controller');
const NewsAPI = require('newsapi');
const newsapi = new NewsAPI('b21e32ef5ea7482eaf4231b9dd56e828');
let news = [];

const get_news = () => {
	newsapi.v2.everything({
		q: 'stock',
		language: 'en',
		sortBy: 'publishedAt'
	}).then(response => {
		if(response.status === "ok") {
			news = response.articles;
			redis_promise_controller.set(config.get('redis_chanel.news_feed'), JSON.stringify(news));
			log.info(news);
		}
		else {
			log.error(response)
		}
	});
};
get_news();
setInterval(get_news, 2*60*1000);