/**
 * Created by trung.than on 2018/12/18.
 */
const crypto = require('crypto');
const Enum = require('enum');
const speakeasy = require('speakeasy');

const algorithm 	= 'aes-256-ctr';
const password 	= 'whyyouwantNINJAtoknow2018password';

exports.parseJson = function(data) {
	try {
		return JSON.parse(data)
	}
	catch (err) {
		throw error.PARSE_JSON_FAILURE;
	}
};
// error enum
var error = new Enum({
	NO_ERROR: 0,
	PARSE_JSON_FAILURE: 1,
	DATABASE_ERROR: 2,
	NETWORK_ERROR: 3,
    UNKNOWN_ERR: 4,
    REDIS_ERR: 5,
    PERMISSION_DENIED: 6,
    SERVER_MAINTAIN: 7,
    CHANNEL_INVALID: 8,
    NOT_ENOUGH_BALANCE: 9,
    WRONG_PURCHASE_TYPE:10
	// order api
});
exports.error = error;

// get error message
var getErrorMessage = function(error_code) {
	switch (error_code) {
		case (error.NO_ERROR) :
			return 'No error!';
			break;
		default:
			return error.get(error_code).key;
			break;
	}
};
exports.getErrorMessage = getErrorMessage;

module.exports.saltAndHash = function(pass) {
    var salt = generateSalt();
    var hash = salt + sha256(pass + salt);
    return hash;
};

module.exports.validatePassword = function(plainPass, hashedPass)
{
    var salt = hashedPass.substr(0, 10);
    var validHash = salt + sha256(plainPass + salt);
    return (hashedPass === validHash);
};

module.exports.md5 = function (str) {
    return md5(str);
};

module.exports.sha256 = function (str) {
    return sha256(str);
};

module.exports.randomToken = function () {
    return randomBytes();
};

var encrypt = function (text, pass) {
    //console.log(text, pass);
    if(!pass)
        pass = password;
    var cipher = crypto.createCipher(algorithm,pass);
    return cipher.update(text,'utf8','hex') + cipher.final('hex');
};
module.exports.encrypt = encrypt;

var decrypt = function (cryptedText, pass) {
    //console.log(cryptedText, pass);
    try {
        if(!pass)
            pass = password;
        var decipher = crypto.createDecipher(algorithm,pass);
        return decipher.update(cryptedText,'hex','utf8') + decipher.final('utf8');
    }
    catch (e) {
        throw error.TOKEN_INVALID;
    }
};

module.exports.decrypt = decrypt;

module.exports.removeQuotes = function(data) {
    if (data == null)
        return ("");

    var new_data = String(data);
    return new_data.replace(/['"]+/g, '');
};

//---------------------------------------private function---------------------------------------------------------------

var generateSalt = function (){
    var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
    var salt = '';
    for (var i = 0; i < 10; i++) {
        var p = Math.floor(Math.random() * set.length);
        salt += set[p];
    }
    return salt;
};
module.exports.gen_random_password = generateSalt;

var md5 = function(str) {
    return crypto.createHash('md5').update(str).digest('hex');
};

var sha256 = function (str) {
    return crypto.createHash('sha256').update(str).digest('hex');
};

var randomBytes = function () {
    crypto.randomBytes(64, function (error, buffer) {
        return buffer.toString('hex');
    });
};

module.exports.validatePassword = validatePassword;
function validatePassword(plainPass, hashedPass)
{
    var salt = hashedPass.substr(0, 10);
    var validHash = salt + sha256(plainPass + salt);
    return (hashedPass === validHash);
}
// check google authenticator code
var check_2fa_code = function(code, key) {

    // Use verify() to check the token against the secret
    var verified = speakeasy.totp.verify({ secret: key,
        encoding: 'base32',
        token: code });
    //console.log('check_2fa_code', verified);
    return verified;
};
exports.check_2fa_code = check_2fa_code;

// --- Validate password must be at least 8 characters with uppercase letters, special symbols and numbers
let regex_pass = new RegExp("^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\\d]){1,})(?=(.*[\\W]){1,})(?!.*\\s).{8,}$");
module.exports.checkPasswordMeetRequirement  = function (pw_str) {
    return regex_pass.test(pw_str);
};

// --- Validate username must contain only letters and numbers
let regex_username = /^[a-zA-Z0-9.\-_$@*!]{3,30}$/;
module.exports.checkUsernameMeetRequirement  = function (username_str) {
    return regex_username.test(username_str);
};

// --- Validate email
let regex_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
module.exports.checkEmailMeetRequirement = function(email) {
    return regex_email.test(email);
};

// --- Generate random token SMS with 6 digit
module.exports.generateSMSToken = function (){
    const set = '0123456789';
    let salt = '';
    for (let i = 0; i < 6; i++) {
        const p = Math.floor(Math.random() * set.length);
        salt += set[p];
    }
    return salt;
};

// --- Validate string contain only number
const isnum = /^\d+$/;
module.exports.isNum = function (str) {
    return isnum.test(str);
};

// --- Encrypt phone number
exports.encrypt_phone_number = (phone_number) => {
    if(phone_number.length < 8) return null;
    return phone_number.slice(0,3)+ "**********" + phone_number.slice(phone_number.length - 3,phone_number.length);
};