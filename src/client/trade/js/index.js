/**
 * Created by trung.than on 2018/12/18.
 */

/**
 * Main AngularJS Web Application
 */
var app = angular.module('TradingGui', [
    'ui.router','ui-notification', 'pascalprecht.translate', 'dialogHelper', 'SocketServices',
	'ContainerControllers','HomeControllers','SIOControllers']);
$stateProvider="app";
/**
 * Configure the Routes
 */
app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    // Routing
    $urlRouterProvider.when('', '/');
    $urlRouterProvider.otherwise(function () {
        window.location.href = '/not_found';
    });
    $stateProvider
        .state('app', {
            abstract: true,
            views: {
                '': {
                    templateUrl: 'trade/html/container.html',
                    controller: 'ContainerCtrl'
                }
            }
        })
		.state("app.home",{
			url: '/',
			templateUrl: 'trade/html/home.html',
			controller: 'HomeCtrl'
		})
	}])
	.config(['$locationProvider', function($locationProvider) {
        $locationProvider.html5Mode({
            enabled: false
        });
		// $locationProvider.hashPrefix('');
	}])
	.config(function(NotificationProvider) {
		NotificationProvider.setOptions({
			delay: 5000,
			startTop: 25,
			startRight: 18,
			verticalSpacing: 20,
			horizontalSpacing: 20,
			positionX: 'right',
			positionY: 'bottom'
		});
	})
    .filter('toArray', function() { return function(obj) {
        if (!(obj instanceof Object)) return obj;
        return _.map(obj, function(val, key) {
            val.key = key;
            return Object.defineProperty(val, '$key', {__proto__: null, value: key});
        });
    }})
	.filter('dropDigits', function() {
		return function(floatNum) {
			return String(floatNum)
				.split('.')
				.map(function (d, i) { return i ? d.substr(0, 4) : d; })
				.join('.');
    	};
	})
    .filter('ThousandDelemiter', function() {
        return function(floatNum) {
            return String(floatNum)
                .split('.')
                .map(function (d, i) { return i ? d : d.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); })
                .join('.');
        };
    })
	.filter("trust", ['$sce', function($sce) {
		return function(htmlCode){
			return $sce.trustAsHtml(htmlCode);
		}
	}])
	.directive("compareTo", function () {
		return {
			require: "ngModel",
			scope: {
				otherModelValue: "=compareTo"
			},
			link: function(scope, element, attributes, ngModel) {

				ngModel.$validators.compareTo = function(modelValue) {
					return modelValue == scope.otherModelValue;
				};

				scope.$watch("otherModelValue", function() {
					ngModel.$validate();
				});
			}
		};
	}).directive('stringToNumber', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModel) {
				ngModel.$parsers.push(function(value) {
					return '' + value;
				});
				ngModel.$formatters.push(function(value) {
					return parseFloat(value);
				});
			}
		};
	}).directive('fileReader', function() {
		return {
			scope: {
				fileReader:"="
			},
			link: function(scope, element) {
				$(element).on('change', function(changeEvent) {
					var files = changeEvent.target.files;
					if (files.length) {
						var r = new FileReader();
						r.onload = function(e) {
							var contents = e.target.result;
							scope.$apply(function () {
								scope.fileReader = contents;
								scope.testing = contents;
							});
						};

						r.readAsText(files[0]);
					}
				});
			}
		};
	})
	.directive('validateNumberConfig', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.on('keypress', function(event) {
					if( event.which != 46  && event.which != 13  &&  event.which != 190 && event.which != 110 && event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57)) {
					    event.preventDefault();
					}
				});
			}
		};
	})
	.directive('validateNumberConfig2', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.on('keypress', function(event) {
					if( event.which != 46  && event.which != 13  &&  event.which != 190 && event.which != 110 && event.which != 8 && event.which != 0 && event.which != 46 && event.which != 47  && (event.which < 45 || event.which > 57)) {
					    event.preventDefault();
					}
				});
			}
		};
	})
	.directive('validateString', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.on('keypress', function(event) {
					if( event.which == 44  || event.which == 34||event.which == 39) {
						event.preventDefault();
					}
				});
			}
		};
	})
	.directive('validationError', function () {
		return {
			restrict: 'A',
			require: 'ngModel',
			link: function (scope, elem, attrs, ctrl) {
				scope.$watch(attrs['validationError'], function (errMsg) {
					if (elem[0] && elem[0].setCustomValidity) { // HTML5 validation
						elem[0].setCustomValidity(errMsg);
					}
					if (ctrl) { // AngularJS validation
						ctrl.$setValidity('validationError', errMsg ? false : true);
					}
				});
			}
		}
	})
	.directive("scroll", function ($window) {
		return function(scope, element, attrs) {
			angular.element($window).bind("scroll", function() {
				console.log('aaa');
			});
		};
	})
	.directive('whenScrolled', function() {

		return function(scope, elm, attr) {
			console.log('vvv');
			var raw = elm[0];
			elm.bind('scroll', function() {
				if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
					scope.$apply(attr.whenScrolled);
				}
			});
		};
	})
	.directive("focusOn", function($timeout) {
		return {
			restrict: "A",
			link: function(scope, element, attrs) {
				scope.$on(attrs.focusOn, function(e) {
					$timeout((function() {
						element[0].focus();
					}), 10);
				});
			}
		};
	})
	.directive('pwCheck', [function () {
		return {
			require: 'ngModel',
			link: function (scope, elem, attrs, ctrl) {
				var firstPassword = '#' + attrs.pwCheck;
				elem.add(firstPassword).on('keyup', function () {
					scope.$apply(function () {
						ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
					});
				});
			}
		}
	}])
	.directive('sglclick', ['$parse', function($parse) {
		return {
			restrict: 'A',
			link: function(scope, element, attr) {
				var fn = $parse(attr['sglclick']);
				var delay = 300, clicks = 0, timer = null;
				element.on('click', function (event) {
					clicks++;  //count clicks
					if(clicks === 1) {
						timer = setTimeout(function() {
							scope.$apply(function () {
								fn(scope, { $event: event });
							});
							clicks = 0;             //after action performed, reset counter
						}, delay);
					} else {
						clearTimeout(timer);    //prevent single-click action
						clicks = 0;             //after action performed, reset counter
					}
				});
			}
		};
	}])
	.directive('dragMe', [function($parse) {
		return {
			restrict: 'A',
			link: function(scope, elem, attr, ctrl) {
				elem.draggable({
					handle:	'.ui-widget-header',
					create: function(event, ui) {
						WindowManager.addWindow(this);
					},
					start: function( event, ui ) {
						ui.helper.css("z-index", WindowManager.getNextzIndex());
					},
					stop: function(event, ui) {
						WindowManager.snap(ui.helper);
					}
				});
				elem.resizable({
					handles: 'n, e, s, w, se, ne, sw, nw',
					start: function( event, ui ) {
						ui.helper.css("z-index", WindowManager.getNextzIndex());
					},
					stop: function(event, ui) {
						WindowManager.snap(ui.helper);
					}
				});

				elem.click(function() {
					$(this).css("z-index", WindowManager.getNextzIndex());
				});

			}
		};
	}]);



