

angular.module('SocketServices', [])
    .factory('Socket', function ($rootScope) {
    var socket = io.connect({
		reconnection: true,
		reconnectionDelay: 1000,
		reconnectionDelayMax : 5000,
		reconnectionAttempts: 99999
	});
    return {
		on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data) {
            return new Promise((resolve, reject) => {
                if (!socket) {
                    reject('No socket connection.');
                } else {
                    console.log('emit ',eventName, data);
                    socket.emit(eventName, data, (response) => {
                        resolve(response);
                    });
                }
            });
        }
    };
});