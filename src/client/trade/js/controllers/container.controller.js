/**
 * Created by trung.than on 2018/12/18.
 */
angular.module('ContainerControllers', ['ngCookies'])
	.controller('ContainerCtrl', function ($scope, $rootScope, Socket, $cookies, $window, dialog_helper, Notification, $translate, $location, $cookies) {
		console.log("ContainerCtrl started");
        $rootScope.sidebar_show = true;
        $rootScope.symbol_sorting = 'symbol';
        $rootScope.symbol_sorting_order = false;
		$rootScope.menu_symbol = false;
		$rootScope.alphabetList = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
		$rootScope.news_list = [];

		$scope.hide_menu_symbol = function() {
			$rootScope.menu_symbol = false;
			if (!$rootScope.$$phase) $rootScope.$apply();
		};

		$scope.toggle_menu_symbol = function() {
			$rootScope.menu_symbol = !$rootScope.menu_symbol;
			console.log($rootScope.menu_symbol);
			if (!$rootScope.$$phase) $rootScope.$apply();
		};

		$scope.update_new_initial = async function(initial){
			//console.log('Current initial: ', initial);
			//Disable selection while waiting for result
			$rootScope.disableInitialSelection = true;

			let response = await Socket.emit('symbol_list',{initial: initial});

			if(response.result_code) {
				Notification.error({
					title: "Getting symbol list ERROR",
					message: response.error_message
				});
			}
			else {
				$rootScope.symbol_list = response.data;
				//console.log('symbol._list', $rootScope.symbol_list);
			}

			$rootScope.disableInitialSelection = false;
		};

		$scope.change_symbol = async function(symbol) {
			//console.log('New symbol: ', symbol)
			//Disable selection while waiting for result
			$rootScope.disableSymbolSelection = true;

			let res = await Socket.emit('set_currency_symbol', symbol);
			if(res.result_code) {
				Notification.error({
					message: res.error_message
				});
			}

			$rootScope.current_symbol = res.data;
			$rootScope.clock = Date.now();
			$rootScope.disableSymbolSelection = false;
			if (!$rootScope.$$phase) $rootScope.$apply();
        };

        $scope.change_symbol_sorting = function(type) {
            $rootScope.symbol_sorting = type;
            $rootScope.symbol_sorting_order = !$rootScope.symbol_sorting_order;
        };

        $scope.change_balance_sorting = function(type) {
            $rootScope.balance_sorting = type;
            $rootScope.balance_sorting_order = !$rootScope.balance_sorting_order;
        };

		$scope.get_news_list = async () => {
			// $rootScope.news_list = [];
			let news = await Socket.emit('news', {});
			console.log('news', news);
			if(!news.result_code) $rootScope.news_list = news.data;
		};

		setInterval(function(){
			$scope.get_news_list();
			if($rootScope.user) $cookies.put('last_active', Date.now());
		}, 60*1000);
	});
