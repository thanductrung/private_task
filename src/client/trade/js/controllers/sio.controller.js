/**
 * Created by trung.than on 2018/12/18.
 */
angular.module('SIOControllers', ['ngCookies'])
	.controller('SIOCtrl', function ($scope, $rootScope, $timeout, $interval, $filter, $cookies, $location, Socket, Notification) {
		console.log('SIO controller');

        $rootScope.fund_history = [];
        $rootScope.purchase_history = [];

        $scope.sum = function(items, prop){
            return items.reduce( function(a, b){
                return a + b[prop];
            }, 0);
        };

        Socket.on('connect', async () => {
            console.log('socket.io connected!');

            /* GET current symbol price */
            let get_symbol_price = function(){
                Socket.emit('get_currency_symbol_price').then(function(res){
                    if(res.result_code) {
                        Notification.error({
                            title: "Get current symbol price ERROR",
                            message: res.error_message
                        });
                    }

                    $rootScope.current_symbol = res.data;
                    $rootScope.clock = Date.now();
                    if (!$rootScope.$$phase) $rootScope.$apply();
                });
            };
            setInterval(get_symbol_price, 10 * 1000);
            get_symbol_price();


            /* GET balance_list */
            Socket.emit('get_balance_list',{}).then(function(response){
                console.log('get_balance_list response', response);

                if(response.result_code) {
                    Notification.error({
                        title: "Get balance ERROR",
                        message: response.error_message
                    });
                }
                else {
                    $rootScope.balance_list = response.data;
                    $rootScope.total_asset = $scope.sum($rootScope.balance_list, 'usd_balance');
                    if (!$rootScope.$$phase) $rootScope.$apply();
                }
            });


            /* GET fund_history */
            Socket.emit('fund_history',{}).then(function(response){
                console.log('fund_history response', response);

                if(response.result_code) {
                    Notification.error({
                        title: "Get fund history ERROR",
                        message: response.error_message
                    });
                }
                else {
                    $rootScope.fund_history = response.data;
                    if (!$rootScope.$$phase) $rootScope.$apply();
                }
            });

            /* GET fund_history */
            Socket.emit('purchase_history',{}).then(function(response){
                console.log('purchase_history response', response);

                if(response.result_code) {
                    Notification.error({
                        title: "Get purchase history ERROR",
                        message: response.error_message
                    });
                }
                else {
                    $rootScope.purchase_history = response.data;
                    if (!$rootScope.$$phase) $rootScope.$apply();
                }
            });

            // Get news
            $scope.get_news_list();


            /* GET symbol_list with initial */
            $rootScope.curInitial = 'A';
            let response = await Socket.emit('symbol_list',{initial: $rootScope.curInitial});
            //console.log('symbol_list response', response);

            if(response.result_code) {
                Notification.error({
                    title: "Getting symbol list ERROR",
                    message: response.error_message
                });
            }
            else {
                $rootScope.symbol_list = response.data;
                //console.log('symbol._list', $rootScope.symbol_list);
            }
        });

        Socket.on('disconnect', async function () {
            console.error('socket.io disconnected!');
        });
	});