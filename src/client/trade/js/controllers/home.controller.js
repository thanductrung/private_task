/**
 * Created by trung.than on 2018/12/18.
 */
angular.module('HomeControllers', ['ngCookies'])
    .controller('HomeCtrl', function ($scope, $rootScope, Socket, dialog_helper, Notification) {
        $scope.Math = window.Math;
        console.log('HomeCtrl started');
		$rootScope.menu_symbol = false;

        $scope.update_usd_balance = function(type, amount) {
            let confirm_string = "";
            if (type === "deposit") {
                confirm_string = ["Are you sure to deposit ", amount, " USD to your wallet?"].join("");
            }
            else if (type === "withdraw") {
                let usd_obj = $rootScope.balance_list.getItemBy("symbol","USD");
                if (usd_obj.balance < amount) {
                    Notification.error({
                        title: "Withdraw USD ERROR",
                        message: "Not enough balance"
                    });
                    return;
                }

                confirm_string = ["Are you sure to withdraw ", amount, " USD from your wallet?"].join("");
            }
            else
                return;

            dialog_helper.confirm(confirm_string, function (ok) {
                if (ok) {
                    $rootScope.disableFundButton = true;
                    if (!$rootScope.$$phase) $rootScope.$apply();

                    /* Send update request */
                    Socket.emit('update_usd_balance',{
                        amount: (type === "deposit") ? amount : -amount
                    }).then(function(response){
                        console.log('update_usd_balance response', response);

                        if(response.result_code) {
                            Notification.error({
                                title: "Update balance ERROR",
                                message: response.error_message
                            });
                        }
                        else {
                            $rootScope.balance_list = response.data;
                            $rootScope.total_asset = $scope.sum($rootScope.balance_list, 'usd_balance');

                            $rootScope.fund_history.unshift({
                                type: (type === "deposit") ? "Deposit" : "Withdraw",
                                amount: amount,
                                time: Date.now()
                            });

                            if ($rootScope.length > 100)
                                $rootScope.fund_history.splice(-1,1);

                            $rootScope.disableFundButton = false;
                            if (!$rootScope.$$phase) $rootScope.$apply();
                        }
                    });
                }
            }, false);
        };

        $scope.purchase = function(type, amount, symbol) {
            let confirm_string = "";
            if (type === "buy") {
                let usd_obj = $rootScope.balance_list.getItemBy("symbol","USD");
                if (!usd_obj || usd_obj.balance < amount * symbol.price) {
                    Notification.error({
                        title: "Purchase stock ERROR",
                        message: "Not enough balance"
                    });
                    return;
                }

                confirm_string = ["Are you sure to buy ", amount, " ", symbol.symbol, "?"].join("");
            }
            else if (type === "sell") {
                let stock_obj = $rootScope.balance_list.getItemBy("symbol",symbol.symbol);
                if (!stock_obj || stock_obj.balance < amount) {
                    Notification.error({
                        title: "Sell stock ERROR",
                        message: "Not enough balance"
                    });
                    return;
                }

                confirm_string = ["Are you sure to sell ", amount, " ", symbol.symbol, "?"].join("");
            }
            else
                return;

            dialog_helper.confirm(confirm_string, function (ok) {
                if (ok) {
                    $rootScope.disablePurchaseButton = true;
                    if (!$rootScope.$$phase) $rootScope.$apply();

                    /* Send purchase request */
                    Socket.emit('purchase',{
                        type: type,
                        amount: amount,
                        symbol: symbol.symbol,
                        price: symbol.price
                    }).then(function(response){
                        console.log('purchase response', response);

                        if(response.result_code) {
                            Notification.error({
                                title: "Purchase ERROR",
                                message: response.error_message
                            });
                        }
                        else {
                            $rootScope.balance_list = response.data;
                            $rootScope.total_asset = $scope.sum($rootScope.balance_list, 'usd_balance');

                            $rootScope.purchase_history.unshift({
                                type: (type === "buy") ? "Buy" : "Sell",
                                amount: amount,
                                price: symbol.price,
                                symbol: symbol.symbol,
                                time: Date.now()
                            });

                            if ($rootScope.length > 100)
                                $rootScope.fund_history.splice(-1,1);

                            $rootScope.disablePurchaseButton = false;
                            if (!$rootScope.$$phase) $rootScope.$apply();
                        }
                    });
                }
            }, false);
        };
    });
