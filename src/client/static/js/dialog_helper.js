/**
 * Created by Jella on 11/12/2015.
 */
var dialogHelper = angular.module('dialogHelper', []);

dialogHelper.factory('dialog_helper',['$rootScope', function ($rootScope) {
    return {
        confirm: function (message, callback, dont_show_again) {
            $('#popup-content').html(message);
            $('#popup').addClass('show');
            $('#confirm_cancel').show();
            if(dont_show_again)
                $('#dont_show_again').show();
            else
                $('#dont_show_again').hide();
            this.dialog(message, callback);
        },
        message: function (message, callback, dont_show_again) {
            $('#popup-content').html(message);
            $('#confirm_cancel').hide();
            $('#popup').addClass('show');
            if(dont_show_again)
                $('#dont_show_again').show();
            else
                $('#dont_show_again').hide();
            this.dialog(message, callback);
        },
        dialog: function (message, callback) {
            $('#confirm_ok').focus();
            $('#confirm_ok').unbind( "click" );
            $('#confirm_cancel').unbind( "click" );
            $('#confirm_ok').click(function(){
                $('#popup').removeClass('show');
                if (callback) callback(true,  $('#dont_show_again_input').prop('checked'));
            });
            $('#confirm_cancel').click(function(){
                $('#popup').removeClass('show');
                if (callback) callback(false,  $('#dont_show_again_input').prop('checked'));
            });
            $('#dont_show_again_input').prop('checked', false);
            this.focus('ok');
        },
        focus: function (type){
            if(type == 'cancel'){
				$('#confirm_cancel').focus();
            }
            if(type == 'ok'){
				$('#confirm_ok').focus();
            }
        }
    };
}]);
