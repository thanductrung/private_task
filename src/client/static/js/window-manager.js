/*
    Windows Manager
*/


function WindowManager() {
	this.aWindows = [];
	this.nNextzIndex = 1;
}

WindowManager.prototype.init = function() {

}

WindowManager.prototype.getNextzIndex = function() {
	return ++this.nNextzIndex;
}

WindowManager.prototype.snap = function(win) {
	console.log(win);

	var x = 0;
	var y = 0;
	var dx = 0;
	var dy = 0;

	var winOffset = win.offset();
	var winWidth = win.width();
	var winHeight = win.height();
	var threshold = 10;
	// console.log("win", winOffset, winWidth, winHeight);

	var aX = [];
	var aY = [];

	for(var i=0; i<this.aWindows.length; i++) {
		var o = this.aWindows[i];
		var offset = o.offset();
		var width = o.width();
		var height = o.height();
		// console.log(i, offset, width, height);

		aY.push({dis:Math.abs(offset.top - winOffset.top), dY: (offset.top - winOffset.top)});
		aY.push({dis:Math.abs(offset.top - (winOffset.top+winHeight)), dY: offset.top - (winOffset.top+winHeight)});
		aY.push({dis:Math.abs(offset.top+height - winOffset.top), dY: offset.top+height - winOffset.top});
		aY.push({dis:Math.abs(offset.top+height - (winOffset.top+winHeight)), dY: offset.top+height - (winOffset.top+winHeight)});

		aX.push({dis:Math.abs(offset.left - winOffset.left), dX: offset.left - winOffset.left});
		aX.push({dis:Math.abs(offset.left - (winOffset.left + winWidth)), dX: offset.left - (winOffset.left + winWidth)});
		aX.push({dis:Math.abs(offset.left+width - winOffset.left), dX: offset.left+width - winOffset.left});
		aX.push({dis:Math.abs(offset.left+width - (winOffset.left + winWidth)), dX: offset.left+width - (winOffset.left + winWidth)});
	}

	// console.log(aX, aY);
	aX.sort(function(a, b) {	return a.dis-b.dis;	});
	aY.sort(function(a, b) {	return a.dis-b.dis;	});

	// console.log(aX, aY);
	var dy = 0;
	var dx = 0;

	for(var i=0; i< aX.length; i++) {
		if(1<= aX[i].dis && aX[i].dis < threshold) {
			dx = aX[i].dX;
			break;
		}
	}

	for(var i=0; i< aY.length; i++) {
		if(1<=aY[i].dis && aY[i].dis < threshold) {
			dy = aY[i].dY;
			break;
		}
	}


	if(dy != 0 || dx!= 0) {
		win.offset({top: winOffset.top + dy, left: winOffset.left + dx});
	}
}

WindowManager.prototype.addWindow = function(win) {
	this.aWindows.push($(win));
}

var WindowManager = new WindowManager();