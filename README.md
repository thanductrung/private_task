# Qwilr take-home task

## Prerequesites

You will need:

- [Node](https://nodejs.org)

## Download source code
```sh
git clone https://thanductrung@bitbucket.org/thanductrung/private_task.git
cd private_task
```

## Install
Install dependencies:
```sh
npm install
```

Install PM2:
```sh
npm install pm2 -g
```

## Run
```sh
pm2 start app.config.js
```

## Front-end
You can check the result at [Localhost](http://localhost:8000)

## Demo
If you don't want to run the above steps, have a look at hosted demo at [Demo](htpp://trungthan.pro:8000)