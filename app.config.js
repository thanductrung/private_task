let environment = "production";
module.exports = {
    apps : [
        {
            name      : 'app',
            script    : './src/app/init.js',
            max_memory_restart : '500M',
            env: {
                "NODE_ENV": environment,
            }
        },
        {
            name      : 'news',
            script    : './src/news_feed/init.js',
            max_memory_restart : '500M',
            env: {
                "NODE_ENV": environment,
            }
        }
    ]
};
